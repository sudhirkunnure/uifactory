import React from 'react';
import PropTypes from 'prop-types';
import { DropdownButton, MenuItem } from "react-bootstrap";

const SimpleCombobox = props => {
    return (
      
        <div>
            <DropdownButton title={'Select User'} bsStyle={'primary'} id={'primaryButton'}>
                <MenuItem eventKey="1">Akshay</MenuItem>
                <MenuItem eventKey="2">Prasad</MenuItem>
                <MenuItem eventKey="3">Tanima</MenuItem>
            </DropdownButton>
        </div>
    );
  
};

SimpleCombobox.propTypes = {

};

export default SimpleCombobox;