export function createReducer(initialState, reducerMap) {
    return (state = initialState, action = {}) => {
        const reducer = reducerMap[action.type];

        return reducer
            ? reducer(state, action.payload)
            : state;
    };
}

export function checkHttpStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        throw response;
    }
}

export function parseJSON(response) {
    try {
        return response.json();
    }
    catch (e) {
        console.error(response.stack);
        throw e;
    }
}