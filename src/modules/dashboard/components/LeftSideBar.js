import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SimpleCombobox from '../../formfields/comboBox/components/simpleCombobox';
import TextBoxField from '../../formfields/textBox/components/textBoxField';
import Header from 'modules/dashboard/components/Header';
import { Link } from "react-router";
import { Grid, Row, Col, Button, ButtonGroup, bsS } from 'react-bootstrap';

class LeftSideBar extends Component {
    constructor(props) {
        super();
    }
    render() {
        return (
            <div>
                <Header />

                <div>
                    <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div className="panel panel-default">
                            <div className="panel-heading" role="tab" id="headingOne">
                                <h4 className="panel-title">
                                    <div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Form Fields
                     </div>
                                </h4>
                            </div>
                            <div id="collapseOne" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div className="panel-body">
                                    <ul className="header">

                                        <li><Link to={"/combo"}>Combo</Link></li>
                                        <li><Link to={"/table"}>Table</Link></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default">
                            <div className="panel-heading" role="tab" id="headingTwo">
                                <h4 className="panel-title">
                                    <div className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Component 2
        </div>
                                </h4>
                            </div>
                            <div id="collapseTwo" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div className="panel-body">
                                    Hello
      </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

LeftSideBar.propTypes = {

};

export default LeftSideBar;

