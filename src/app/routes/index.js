// import React from 'react';
// import {IndexRoute, Router, Route, useRouterHistory} from 'react-router';
// import {createHashHistory} from 'history';
// import {syncHistoryWithStore} from 'react-router-redux';
// import Dashboard from 'modules/Dashboard/components/Dashboard';
// import PageNotFoundError from 'modules/Dashboard/components/PageNotFoundError';
// import SimpleCombobox from 'modules/formfields/comboBox/components/simpleCombobox';
// import store from 'app/store';
// const appHistory = useRouterHistory(createHashHistory)({queryKey: false});
// const history = syncHistoryWithStore(appHistory, store);

// const Routes = (
//     <Router history={history}>
//         <Route path="/" component={Dashboard}>
//             <Route path="/simpleCombobox" component={SimpleCombobox}/>
//             <Route path="*" component={PageNotFoundError}/>
//         </Route>
//     </Router>
// );

// export default Routes;

import React from 'react';
import { IndexRoute, Router, Route, useRouterHistory } from 'react-router';
import { createHashHistory } from 'history';
import { syncHistoryWithStore } from 'react-router-redux';
import Dashboard from '../../modules/Dashboard/components/Dashboard';
import PageNotFoundError from 'modules/Dashboard/components/PageNotFoundError';
import SimpleCombobox from '../../modules/formfields/comboBox/components/simpleCombobox';
import TextBoxField from '../../modules/formfields/textBox/components/textBoxField';
import store from 'app/store';
const appHistory = useRouterHistory(createHashHistory)({ queryKey: false });
const history = syncHistoryWithStore(appHistory, store);

const Routes = (
    <Router history={history}>
        <Route path="/" component={Dashboard}>     
            <IndexRoute component={Dashboard} />       
            <Route path={"combo"} component={SimpleCombobox}> </Route>
              <Route path={"table"} component={TextBoxField}> </Route>  
             <Route path="*" component={PageNotFoundError} ></Route>  
            </Route>      
    </Router>
);

export default Routes;

