const argv = require('minimist')(process.argv.slice(2));
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const distPath = 'dist';
const src = path.join(__dirname, 'src');

const config = {
    entry: {
        app: ["babel-polyfill", src],
        vendor: [
            "jquery",
            "bootstrap",
            "history",
            "http-proxy",
            "isomorphic-fetch",
            "keykey",
            "lodash",
            "react",
            "react-bootstrap",
            "react-dom",
            "react-redux",
            "react-redux-toastr",
            "react-router",
            "react-router-redux",
            "redux",
            "redux-logger",
            "redux-thunk"
        ]
    },

    output: {
        filename: '[name].min.[chunkhash].js',
        path: path.join(__dirname, distPath),
        publicPath: ''
    },

    resolve: {
        root: src,
        extensions: ['', '.js', '.jsx']
    },

    devtool: false,

    devServer: {
        hot: true,
        inline: true,
        historyApiFallback: true,
        contentBase: path.join(__dirname, distPath),
        compress: true
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loaders: [
                    'react-hot',
                    'babel?presets[]=react,presets[]=es2015'
                ]
            },
            {
                test: /.css$/,
                loader: 'style!css!'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'url?limit=10000',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader?limit=100000'
            }
        ]
    },

    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(true),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compressor: {
                warnings: false
            }
        }),
        new webpack.optimize.DedupePlugin(),
        new HtmlWebpackPlugin({
            template: path.join('src', 'index.html'),
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true,
            },
            inject: true,
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'commons',
            filename: 'commons.min.[chunkhash].js',
            minChunks: 2,
        }),

        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
    ]
};

module.exports = config;